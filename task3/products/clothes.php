<?php
namespace task3\products;

use task3\abstractclass\Product;

class Clothes extends Product{
    private $size;

    public function __construct($weight, $price, $sku, $size)
    {
        parent::__construct($weight, $price, $sku);
        $this->setShipping();
        $this->setSize($size);
        $this->setCategory('clothes');
    }

    public function validProduct()
    {
        if(empty($this->getSize())) {
            throw new \Exception('Empty size');
        }
    }

    public function setSize($size)
    {
        $this->size = $size;
    }

    public function getSize()
    {
        return $this->size;
    }
}