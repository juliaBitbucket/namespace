<?php
namespace task3\products;

use task3\abstractclass\Product;

class Toys extends Product{
    private $color;
    private $age;

    public function __construct($weight, $price, $sku, $age)
    {
        parent::__construct($weight, $price, $sku);
        $this->setAge($age);
        $this->setDiscountPercent(10);
        $this->setShipping();
        $this->setCategory('toys');

    }


    public function validProduct()
    {
        if(empty($this->getAge())) {
            throw new \Exception('Empty age');
        }
    }

    public function setColor($color)
    {
        $this->color = $color;
    }

    public function getColor()
    {
        return $this->color;
    }

    public function setAge($age)
    {
        $this->age = $age;
    }

    public function getAge()
    {
        return $this->age;
    }
}