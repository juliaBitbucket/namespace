<?php
namespace task3\products;

use task3\abstractclass\Product;

class Device extends Product{
    private $type;
    private $brand;
    private $maxWeight = 10;

    public function __construct($weight, $price, $sku, $type, $brand)
    {
        parent::__construct($weight, $price, $sku);
        $this->setDiscountPercent(10);
        $this->setShipping();
        $this->setBrand($brand);
        $this->setType($type);
        $this->setCategory('device');
    }

    public function validProduct()
    {
        if(empty($this->getType())) {
            throw new \Exception('Empty type');
        }

        if(empty($this->getBrand())) {
            throw new \Exception('Empty brand');
        }
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setBrand($brand)
    {
        $this->brand = $brand;
    }

    public function getBrand()
    {
        return $this->brand;
    }

    public function setDiscountPercent($discount)
    {
        if ($this->getWeight() > $this->maxWeight) {
            parent::setDiscountPercent($discount);
        }
    }
}