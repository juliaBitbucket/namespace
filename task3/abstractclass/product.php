<?php
namespace task3\abstractclass;

use task3\interfac\ProductInterface;

abstract class Product implements ProductInterface{
    const SHIPPING = 250;
    const SHIPPING_DISCOUNT = 300; // shipping if discount

    protected $sku;
    protected $price;
    protected $category;
    protected $weight;
    protected $discountPercent = 0;
    protected $discount = 0;
    protected $shipping = self::SHIPPING;
    protected $total;

    public function __construct($weight, $price, $sku)
    {
        $this->setWeight($weight);
        $this->setPrice($price);
        $this->setSku($sku);
        $this->setDiscount();
    }

    abstract public function validProduct();

    public function setCategory($category)
    {
        $this->category = $category;
    }

    public function getCategory()
    {
        return $this->category;
    }

    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    public function getWeight()
    {
        return $this->weight;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price)
    {
        $this->price = $price;
    }

    public function setDiscountPercent($discountPercent)
    {
        $this->discountPercent = $discountPercent;
    }

    public function getDiscountPercent()
    {
        return $this->discountPercent;
    }

    public function setDiscount()
    {
        $this->discount = round($this->getPrice() * $this->getDiscountPercent()/100);
    }

    public function getDiscount()
    {
        return $this->discount;
    }

    public function setShipping()
    {
        if ($this->getDiscountPercent() > 0) {
            $this->shipping = self::SHIPPING_DISCOUNT;
        } else {
            $this->shipping = self::SHIPPING;
        }
    }

    public function getShipping()
    {
        return $this->shipping;
    }

    public function getTotal()
    {
        return $this->getPrice() - $this->getDiscount() + $this->getShipping();
    }

    public function getSku()
    {
        return $this->sku;
    }

    public function setSku($sku)
    {
        $this->sku = $sku;
    }
}
