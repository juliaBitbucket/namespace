<?php
namespace task3\orderproduct;

use task3\abstractclass\Product;

class Cart {
    protected static $products = array();
    protected static $totalQty = 0;
    protected static $totalPrice = 0;

    public function addProduct(Product $product, $qty)
    {
        if (empty($product->getPrice())) {
            throw new \Exception('Empty price for product ' . $product->getCategory());
        }

        if (empty($qty)) {
            throw new \Exception('Empty qty for product ' . $product->getCategory());
        }

        if (empty($product->getSku())) {
            throw new \Exception('Empty sku for product ' . $product->getCategory());
        }

        static::$products[$product->getSku()] = ['product' => $product, 'qty' => $qty];
    }

    public function getProducts()
    {
        return static::$products;
    }

    public function deleteProduct(Product $product, $qty)
    {
        if (empty($product->getPrice())) {
            throw new \Exception('Empty price for product ' . $product->getCategory());
        }

        if (empty($qty)) {
            throw new \Exception('Empty qty for product ' . $product->getCategory());
        }

        if (empty($product->getSku())) {
            throw new \Exception('Empty sku for product ' . $product->getCategory());
        }

        if (empty(static::$products)) {
            throw new \Exception('Empty cart.');
        }

        foreach (static::$products as $sku => $productCart) {
            if ($sku == $product->getSku()) {
                $newQty = $productCart['qty'] - $qty;
                if ($newQty <= 0) {
                    unset(static::$products[$sku]);
                } else {
                    static::$products[$sku]['qty'] = $newQty;
                }
            }
        }
    }

    public static function emptyCart()
    {
        static::$products = array();
        static::$totalPrice = 0;
        static::$totalQty = 0;
    }

    public static function getTotal()
    {
        if (empty(static::$products)) {
            throw new \Exception('Empty cart.');
        }

        $total = 0;
        foreach (static::$products as $sku => $productCart) {
            $total += $productCart['product']->getTotal() * $productCart['qty'];
        }

        return $total;
    }

    public static function getQty()
    {
        if (empty(static::$products)) {
            throw new \Exception('Empty cart.');
        }

        $qty = 0;
        foreach (static::$products as $sku => $productCart) {
            $qty += $productCart['qty'];
        }

        return $qty;
    }
}