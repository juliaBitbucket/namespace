<?php
namespace task3\orderproduct;

use task3\orderproduct\Cart;

class Order {
    private $cart;

    public function __construct(Cart $cart)
    {
        $this->cart = $cart;
    }

    public function printOrder()
    {
        if (empty($this->cart)) {
            throw new \Exception('Empty cart.');
        }

        $style = 'border: 1px solid #ccc; padding: 5px;';
        $print = '<table>
                    <tr>
                        <th style="' . $style . '">Sku</th>
                        <th style="' . $style . '">Qty</th>
                        <th style="' . $style . '">Unit price</th>
                    </tr>';
        $discount = 0;
        $shipping = 0;

        foreach ($this->cart->getProducts() as $product) {
            $print .= '<tr>';
            $print .= '<td style="' . $style . '">' . $product['product']->getSku() . '</td>';
            $print .= '<td style="' . $style . '">' . $product['qty'] . '</td>';
            $print .= '<td style="' . $style . '">' . $product['product']->getPrice() . '</td>';
            $print .= '</tr>';

            $discount += $product['product']->getDiscount();
            $shipping += $product['product']->getShipping();
        }
        $print .= '</table>';

        $print .= '<div>Total discount: ' . $discount . '</div>';
        $print .= '<div>Total shipping: ' . $shipping . '</div>';
        $print .= '<div>Total price: ' . $this->cart->getTotal() . '</div>';

        return $print;
    }
}