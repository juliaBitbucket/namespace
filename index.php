<?php

require_once 'autoloader.php';


use task3\products\Toys;
use task3\products\Device;
use task3\products\Clothes;
try {
    //  скидка 10%
    $toys = new Toys(2, 890, 'toys11324', 3);
    $toys->validProduct();
    $toys->setDiscount();
    $priceToys = $toys->getPrice();
    $discountToys = $toys->getDiscount();
    $shippingToys = $toys->getShipping();
    $totalToys = $toys->getTotal();

// скидка 10% если вес больше 10 кг
    $device = new Device(11, 3160, 'device3423', 'TV', 'Samsung');
    $device->setDiscount();
    $priceDevice = $device->getPrice();
    $discountDevice = $device->getDiscount();
    $shippingDevice = $device->getShipping();
    $totalDevice = $device->getTotal();

// продукт без скидки
    $clothes = new Clothes(1, 1510, 'clothes73889', 'M');
    $clothes->setDiscount();
    $priceClothes = $clothes->getPrice();
    $discountClothes = $clothes->getDiscount();
    $shippingClothes = $clothes->getShipping();
    $totalClothes = $clothes->getTotal();

    $cart = new \task3\orderproduct\Cart();
    $cart->addProduct($toys, 4);
    $cart->addProduct($device, 3);
    $cart->addProduct($clothes, 2);
    $cart->deleteProduct($toys, 2);
    $cart->deleteProduct($device, 1);
    $cart->deleteProduct($clothes, 1);

    //echo 'Total amount:' . $cart->getTotal() . "\n";
    //echo 'Total qty:' . $cart->getQty() . "\n";

    $print = new \task3\orderproduct\Order($cart);
    echo $print->printOrder();
    exit;
} catch (\Exception $e) {
    echo $e->getMessage();
    exit;
}


echo 'Price Toys: ' .$priceToys . ' Discount: ' . $discountToys . ' Shipping: ' . $shippingToys . ' Total: ' . $totalToys . "\n";
echo 'Price Device: ' .$priceDevice . ' Discount: ' . $discountDevice . ' Shipping: ' . $shippingDevice . ' Total: ' . $totalDevice .  "\n";
echo 'Price Clothes: ' .$priceClothes . ' Discount: ' . $discountClothes . ' Shipping: ' . $shippingClothes . ' Total: ' . $totalClothes . "\n";
exit;

